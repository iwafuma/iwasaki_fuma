<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<br /> <label for="login_id">ログインID</label> <input name="login_id"
				id="login_id" />（文字入力）<br /> <label for="branch_id">支店 </label> <input
				name="branch_id" id="branch_id" />（数字記入） <br /> <label
				for="position_id">部署・役職 </label> <input name="position_id"
				id="position" />（数字記入） <br /> <label for="is_stopped">ストップ
			</label> <input name="is_stopped" id="is_stopped" /> （文字入力）<br /> <label
				for="password">パスワード </label> <input name="password" id="password" />（文字入力）
			<br /> <label for="name">ユーザー名 </label> <input name="name" id="name" />（数字記入）
			<br /> <br /> <input type="submit" value="登録" /> <br /> <a
				href="./">戻る</a>
		</form>
		<div class="copyright">Copyright(c)Your Name</div>
	</div>
</body>
</html>